from abstractTypes.CsvDataT import CsvDataT


class PirateBayT(CsvDataT):
    """
    Type for Pirate Bay Torrent data analysis
    """

    def __init__(self):
        CsvDataT.__init__(self)
        CsvDataT.set_headers(self, ["Date", "Hash", "Name", "ID", "Category"])

    def read_data(self):
        return CsvDataT.read_data(self)
