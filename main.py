"""
Reads the Pirate Bay Torrent Dump Data Set and creates a chart showing the most popular times for a torrent to be
uploaded to the Pirate Bay

Requires:
    pandas
    matplotlib
"""
from projectTypes.PirateBayT import PirateBayT


def main():
    data_t = PirateBayT()
    csv_data = data_t.read_data()
    print(csv_data)

    # TODO
    # 1. Extract the DATE and the TIME. Use these to make two separate charts
    # 2. Create a dictionary for category number to category string, update the DataFrame accordingly
    # 3. Create a parent class for all future dataisbeautiful projects, where each project can extend from that parent
    #    class. For example, this project would extend from that class and overwrite all the methods to do what it needs
    #    to do.


if __name__ == '__main__':
    main()
