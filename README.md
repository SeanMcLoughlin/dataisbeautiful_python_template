# dataisbeautiful_python_template

A python template for creating data visualizations. Most useful for creating posts to [/r/dataisbeautiful](https://reddit.com/r/dataisbeautiful).

Requires [numpy](www.numpy.org), [matplotlib](https://matplotlib.org), and [pandas](https://pandas.pydata.org).
