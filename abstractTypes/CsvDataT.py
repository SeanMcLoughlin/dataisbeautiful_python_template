from abstractTypes.PrettyDataT import PrettyDataT
import os
from glob import glob
import pandas as pd


class CsvDataT(PrettyDataT):
    """
    Parent class for data visualization if the data is in CSV format.
    Extend from this class if your data is in CSV format.
    """

    def __init__(self):
        PrettyDataT.__init__(self)
        self.__csv_list = []
        self.__headers = []

    def read_data(self):
        PrettyDataT.read_data(self)
        path = './data'
        for filename in glob(os.path.join(path, '*.csv')):
            self.__csv_list.append(pd.read_csv(filename, sep=';', names=self.__headers))
        try:
            return pd.concat(self.__csv_list)
        except ValueError:
            print("ERROR: Attempted to concat an empty list. Most likely, your CSV files are in a bad path.\n"
                  "Make sure your CSV files are within the 'data' folder of the project's main directory.")
            exit(1)

    def get_csv_list(self):
        return self.__csv_list

    def set_csv_list(self, csv_list):
        self.__csv_list = csv_list

    def get_headers(self):
        return self.__headers

    def set_headers(self, headers):
        self.__headers = headers
