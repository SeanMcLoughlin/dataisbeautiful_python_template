import abc


class PrettyDataT(abc.ABC):
    """
    Parent class for data visualization. Defines functions to be used for all child classes.
    Only different data storage mediums extend from this class.
    For example, CSV data and photographic data are two subclasses of this class.
    """

    @abc.abstractmethod
    def __init__(self):
        pass

    @abc.abstractmethod
    def read_data(self):
        pass
